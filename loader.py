__author__ = 'm_antipov'
import numpy
import struct
import binascii
"""newmtl
Start a definition of a new material
Ka
ambient color (r,g,b)
Kd
diffuse color (r,g,b)
Ks
specular color (r,g,b)
illum
Define the illumination model: illum = 1 a flat material with no specular highlights, illum = 2 denotes the presence of specular highlights
Ns
shininess of the material
d or Tr
the transparency of the material
map_Ka
"""

class TGAHeader:
    HEADER_FMT = "=BBBhhBhhhhBB"
    def __init__(self, data):
        if len(data) >= struct.calcsize(TGAHeader.HEADER_FMT):
            self.idLength, self.colormapType, self.imageType, self.colorMapStartIndex, self.colorMapLength,\
            self.colorMapBits, self.xOrigin, self.yOrigin, self.width, self.height, self.bitsPerPixel, self.flags = struct.unpack_from(TGAHeader.HEADER_FMT, data)
        else:
            raise "Wrong tga data!"
    def size(self):
        return struct.calcsize(TGAHeader.HEADER_FMT)

class TexData:
    def __init__(self, w, h, bpp, data):
        self.width = w
        self.height = h
        self.bpp = bpp
        self.data = data

class ObjModelMtl:
    def __init__(self):
        self.Ka = numpy.empty(3, dtype=numpy.dtype(numpy.float32))
        self.Kd = numpy.empty(3, dtype=numpy.dtype(numpy.float32))
        self.Ks = numpy.empty(3, dtype=numpy.dtype(numpy.float32))
        self.Ke = numpy.empty(3, dtype=numpy.dtype(numpy.float32))
        self.illum = float(0)
        self.Ns = float(0)
        self.Ni = float(0)
        self.d = float(0)
        self.Tr = float(0)
        self.Tf = numpy.empty(3, dtype=numpy.dtype(numpy.float32))
        self.map_Kd = None

class ObjModelGroup:
    def __init__(self, name, start, end):
        self.name = name
        self.start = start
        self.end = end
        self.mat = None

class ObjModelStreamData:
    def __init__(self):
        self.name = ""
        self.stride = 0
        self.elementSize = 0
        self.numElements = 0
        self.stream = None

class ObjModel:
    def __init__(self):
        self.streams = []
        self.num_verts = 0
        self.verts = None
        self.tex_coords = None
        self.normals = None
        self.strides = {}
        self.sizes = {}
        self.groups = []

def loadTGAFile(filename):

        file = open(filename, "rb")
        data = file.read()
        file.close()

        header = TGAHeader(data)

        if header.imageType != 2 and header.imageType != 3:
            return None
        if header.flags & (1 << 4):
            return None

        flipVertical = False
        if header.flags & (1 << 5):
            flipVertical = True

        dataOffs = header.idLength + header.colorMapLength * ((header.colorMapBits + 7) / 8) + header.size()

        width = header.width
        height = header.height
        channels = (header.bitsPerPixel + 7) / 8

        dataArray = numpy.fromstring(data[dataOffs:(dataOffs + (width * height * channels))], dtype=numpy.dtype(numpy.uint8))

        if flipVertical:
            for h in range(0, height / 2):
                for w in range(0, width * channels):
                    temp = dataArray[h * width * channels + w]
                    dataArray[h * width * channels + w] = dataArray[(height - h - 1) * width * channels + w]
                    dataArray[(height - h - 1) * width * channels + w] = temp

        return TexData(header.width, header.height, channels*8, dataArray)

def loadAnyFile(filename):
    import PIL.Image

    try:
        im = PIL.Image.open(filename)
        ix, iy, mode = im.size[0], im.size[1], im.mode

        if mode == "RGB" or mode == "RGBA":
            image = im.tostring("raw", mode, 0, -1)
            return TexData(ix, iy, 32 if mode == "RGBA" else 24, numpy.fromstring(image, dtype=numpy.dtype(numpy.uint8)))
        else:
            return None
    except Exception as e:
        return None

def loadTexFile(filename):
    if filename.endswith(".tga"):
        return loadTGAFile(filename)
    else:
        return loadAnyFile(filename)


def loadMtlLib(filename):
    import os.path
    relPath = os.path.dirname(filename)

    file = open(filename, "r")
    lines = file.readlines()
    file.close()

    materials = {}
    cur_material = None
    for line in lines:
        line = line.strip("\r\n\t ")
        components = line.split(' ', 1)

        if components[0] == "newmtl":
            materials[components[1]] = ObjModelMtl()
            cur_material = materials[components[1]]

        if cur_material:
            if components[0] == "Ns":
                cur_material.Ns = float(components[1])
            if components[0] == "Ni":
                cur_material.Ni = float(components[1])
            if components[0] == "d":
                cur_material.d = float(components[1])
            if components[0] == "Tr":
                cur_material.Tr = float(components[1])
            if components[0] == "illum":
                cur_material.illum = float(components[1])
            if components[0] == "Tf":
                components = components[1].strip("\r\n\t ").split(' ')
                cur_material.Tf = map(float, components)
            if components[0] == "Ka":
                components = components[1].strip("\r\n\t ").split(' ')
                cur_material.Ka = map(float, components)
            if components[0] == "Kd":
                components = components[1].strip("\r\n\t ").split(' ')
                cur_material.Kd = map(float, components)
            if components[0] == "Ks":
                components = components[1].strip("\r\n\t ").split(' ')
                cur_material.Ks = map(float, components)
            if components[0] == "Ke":
                components = components[1].strip("\r\n\t ").split(' ')
                cur_material.Ke = map(float, components)
            if components[0] == "map_Kd":
                cur_material.map_Kd = loadTexFile(relPath + "/" if relPath else "" + components[1])

    return materials

def loadObj(filename, format='vnt'):
        import os.path
        relPath = os.path.dirname(filename)

        file = open(filename, "r")
        lines = file.readlines()
        file.close()

        verts = []
        normals = []
        tex_coords = []
        faces = []
        groups = []
        materials = {}

        cur_group = None

        for line in lines:
            line = line.strip("\r\n\t ")
            components = line.split(' ', 1)

            if components[0] == "mtllib":
                materials = loadMtlLib(relPath + "/" if relPath else "" +components[1])
                break

        faceBlockStarted = False
        for line in lines:
            line = line.strip("\r\n\t ")
            components = line.split(' ')

            if cur_group and faceBlockStarted and not (components[0] in ['s','f', '#']):
                faceBlockStarted = False
                cur_group.end = (len(faces) * 3) - 1

            #skip not used components
            if components[0] == 'v':
                verts.append([float(a) for a in components[1:] if a != ''])
            if components[0] == 'vt':
                tex_coords.append([float(a) for a in components[1:] if a != ''])
            if components[0] == 'vn':
                normals.append([float(a) for a in components[1:] if a != ''])
            if components[0] == 'f':
                faceBlockStarted = True
                for i in range(0, len(components[1:])-2):
                    face = []
                    face.append(components[1 + 0])
                    face.append(components[1 + i + 1])
                    face.append(components[1 + i + 2])
                    faces.append(face)

            if components[0] == 'g':
                cur_group = ObjModelGroup(components[1] if len(components) > 1 else "", len(faces) * 3, 0)
                groups.append(cur_group) # 3 indexes per face
            if components[0] == 'usemtl' and cur_group:
                cur_group.mat = materials.get(components[1], None)

        if cur_group:
            cur_group.end = (len(faces) * 3) - 1
        #parse proper indexes
        indexes = {'v':[], 'n':[], 't':[]}
        for face in faces:
            for component in face:
                indx_vals = component.split('/')
                if len(verts) > 0 and indx_vals[0] != '':
                    indexes['v'].append(int(indx_vals[0]) - 1)
                if len(tex_coords) > 0 and indx_vals[1] != '':
                    indexes['t'].append(int(indx_vals[1]) - 1)
                if len(normals) > 0 and indx_vals[2] != '':
                    indexes['n'].append(int(indx_vals[2]) - 1)

        #check for consistency
        num_v_idx = len(indexes['v'])
        if num_v_idx == 0:
            return None
        else:
            for f in format:
                if len(indexes[f]) != num_v_idx and len(indexes[f]) > 0:
                    return None

        out_verts = []
        out_tex_coords = []
        out_normals = []

        strides = {}
        #sizes = {}

        if len(verts) > 0 and len(indexes['v']) > 0:
            strides['v'] = len(verts[0])
            #sizes['v'] = 4 * strides['v']
            for vi in indexes['v']:
                vert = verts[vi]# if vi < len(verts) else [0 for i in range(0,self.strides['v'])]
                for vv in vert:
                    out_verts.append(vv)

        if len(tex_coords) > 0 and len(indexes['t']) > 0:
            strides['t'] = len(tex_coords[0])
            #sizes['t'] = 4 * strides['t']
            for ti in indexes['t']:
                tex_coord = tex_coords[ti]# if ti < len(tex_coords) else [0 for i in range(0,self.strides['t'])]
                for tt in tex_coord:
                    out_tex_coords.append(tt)

        if len(normals) > 0 and len(indexes['n']) > 0:
            strides['n'] = len(normals[0])
            #sizes['n'] = 4 * strides['n']
            for ni in indexes['n']:
                normal = normals[ni]# if ni < len(normals) else [0 for i in range(0,self.strides['n'])]
                for tt in normal:
                    out_normals.append(tt)

        model = ObjModel()

        if len(out_verts) > 0:
            stream = ObjModelStreamData()
            stream.name = "position"
            stream.elementSize = 4
            stream.stride = strides['v']
            stream.numElements = len(out_verts)
            stream.stream = numpy.array(out_verts, dtype=numpy.float32)
            model.streams.append(stream)

        if len(out_tex_coords) > 0:
            stream = ObjModelStreamData()
            stream.name = "tex_coord"
            stream.elementSize = 4
            stream.stride = strides['t']
            stream.numElements = len(out_tex_coords)
            stream.stream = numpy.array(out_tex_coords, dtype=numpy.float32)
            model.streams.append(stream)

        if len(out_normals) > 0:
            stream = ObjModelStreamData()
            stream.name = "normal"
            stream.elementSize = 4
            stream.stride = strides['n']
            stream.numElements = len(out_normals)
            stream.stream = numpy.array(out_normals, dtype=numpy.float32)
            model.streams.append(stream)

        model.groups = groups

        return model
        #self.indices = numpy.empty(shape=num_v_idx, dtype=numpy.dtype(numpy.uint32))