__author__ = 'm_antipov'

from pyrr import Quaternion, Matrix44, Vector3
import numpy
import math

class Camera:
    def __init__(self, pos):
        self.__pos = pos

        self.__origin = Vector3([0.0, 0.0, 0.0])
        self.__target = Vector3([0.0, 0.0, 1.0])

        self.__right = Vector3([1.0, 0.0, 0.0])
        self.__cameraY = Vector3([0.0, 1.0, 0.0])
        self.__direction = self.__target - self.__origin
        self.__up = Vector3([0.0, 1.0, 0.0])

        self.__rotation = Quaternion([1.0, 0.0, 0.0, 0.0], dtype=numpy.float32)
        #self.__rotation = Matrix44.identity()

    def __getXAxis(self):
        ww = self.__rotation.w * self.__rotation.w
        xx = self.__rotation.x * self.__rotation.x
        yy = self.__rotation.y * self.__rotation.y
        zz = self.__rotation.z * self.__rotation.z
        wy = self.__rotation.w * self.__rotation.y
        wz = self.__rotation.w * self.__rotation.z
        xy = self.__rotation.x * self.__rotation.y
        xz = self.__rotation.x * self.__rotation.z

        return Vector3( [ww + xx - yy - zz, 2.0 * (xy + wz), 2.0 * (xz - wy)] )

    def __getYAxis(self):
        ww = self.__rotation.w * self.__rotation.w
        xx = self.__rotation.x * self.__rotation.x
        yy = self.__rotation.y * self.__rotation.y
        zz = self.__rotation.z * self.__rotation.z
        wx = self.__rotation.w * self.__rotation.x
        wz = self.__rotation.w * self.__rotation.z
        xy = self.__rotation.x * self.__rotation.y
        yz = self.__rotation.y * self.__rotation.z

        return Vector3( [2.0 * (xy - wz), ww - xx + yy - zz, 2.0 * (yz + wx)] )

    def __getZAxis(self):
        ww = self.__rotation.w * self.__rotation.w
        xx = self.__rotation.x * self.__rotation.x
        yy = self.__rotation.y * self.__rotation.y
        zz = self.__rotation.z * self.__rotation.z
        wx = self.__rotation.w * self.__rotation.x
        wy = self.__rotation.w * self.__rotation.y
        xz = self.__rotation.x * self.__rotation.z
        yz = self.__rotation.y * self.__rotation.z

        return Vector3([2.0 * (xz + wy), 2.0 * (yz - wx), ww - xx - yy + zz])

    @property
    def direction(self):
        return self.__direction

    @property
    def position(self):
        return self.__pos

    def rotate(self, yaw, pitch):
        x_axis = Vector3([1.0, 0.0, 0.0])
        y_axis = Vector3([0.0, 1.0, 0.0])

        #self.__rotation = Quaternion.from_axis_rotation(self.__direction, 0.0)
        Quaternion.from_y_rotation(yaw)
        q_rot_y = Quaternion.from_axis_rotation(y_axis, yaw)
        q_rot_y.normalise()
        q_rot_x = Quaternion.from_axis_rotation(x_axis, pitch)
        q_rot_x.normalise()

        self.__rotation = Quaternion.from_y_rotation(yaw) * self.__rotation * Quaternion.from_x_rotation(pitch)
        self.__rotation.normalise()

        #self.__rotation = Matrix44.from_eulers([0.0, pitch, yaw]) * self.__rotation
        #self.__rotation = self.__rotation * Matrix44.from_x_rotation(pitch)

        origin = self.__rotation * self.__origin if self.__origin.length > 0.0 else Vector3([0.0, 0.0, 0.0])
        target = self.__rotation * self.__target

        self.__direction = target - origin
        self.__right = self.__up.cross(self.__direction)
        self.__cameraY = self.__direction.cross(self.__right)
        self.__direction.normalise()
        self.__right.normalise()
        self.__cameraY.normalise()

    def move(self, pos):
        self.__pos = self.__pos + pos

    def viewMatrix(self):
        mat = Matrix44.identity(dtype=numpy.float32)

        mat.m11, mat.m12, mat.m13 = self.__right.x, self.__cameraY.x, self.__direction.x
        mat.m21, mat.m22, mat.m23 = self.__right.y, self.__cameraY.y, self.__direction.y
        mat.m31, mat.m32, mat.m33 = self.__right.z, self.__cameraY.z, self.__direction.z
        mat.m41, mat.m42, mat.m43 = self.__pos.dot(self.__right), self.__pos.dot(self.__cameraY), self.__pos.dot(self.__direction)

        return mat

