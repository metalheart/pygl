__author__ = 'm_antipov'
import OpenGL.GL as GL
import OpenGL.GL.shaders
import ctypes
import pygame
import pygame.mouse
import numpy
import math
from pyrr import Quaternion, Matrix44, Vector3
import cam

vertex_shader = """
#version 330
uniform mat4 proj;
uniform mat4 world;
in vec3 position;
in %s tex_coord;

out vec2 texOut;

void main()
{
   gl_Position = proj * world * vec4(position,1);
   texOut = tex_coord.xy;//tex_coord.xy
}
"""

fragment_shader = """
#version 330
uniform sampler2D tex0;
in vec2 texOut;
void main()
{
   //gl_FragColor = vec4(texOut.x, texOut.y, 1.0f, 1.0f);
   gl_FragColor = texture(tex0, texOut);
}
"""

vertices = [ 0.5,  0.5, 0.0, 1.0,
            -0.5,  0.5, 0.0, 1.0,
             0.0, -0.5, 0.0, 1.0]

vertices = numpy.array(vertices, dtype=numpy.float32)

#matrices and screen
projectionMatrix = None
modelViewMatrix = None

#shader
projMatrixUniform = None
mvMatrixUniform = None

def create_object(shader, model):
    class Dummy:
        pass

    object = Dummy()
    object.vbo = None
    object.tex0sampler = None
    object.parts = []

    object.tex0sampler = GL.glGetUniformLocation( shader, "tex0" )

    # Create a new VAO (Vertex Array Object) and bind it
    object.vbo = GL.glGenVertexArrays(1)
    GL.glBindVertexArray( object.vbo )


    attribs = []
    for stream in model.streams:
        # Generate buffers to hold our vertices
        buffer = GL.glGenBuffers(1)
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, buffer)

        # Get the position of the 'position' in parameter of our shader and bind it.
        stream_attrib = GL.glGetAttribLocation(shader, stream.name)
        if stream_attrib >= 0:
            attribs.append(stream_attrib)
            GL.glEnableVertexAttribArray(stream_attrib)

            # Describe the position data layout in the buffer
            GL.glVertexAttribPointer(stream_attrib, stream.stride, GL.GL_FLOAT, False, 0, ctypes.c_void_p(0))

            # Send the data over to the buffer
            GL.glBufferData(GL.GL_ARRAY_BUFFER, stream.numElements * stream.elementSize, stream.stream, GL.GL_STATIC_DRAW)

    GL.glBindVertexArray( 0 )

    for attrib in attribs:
        GL.glDisableVertexAttribArray(attrib)

    GL.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)

    for group in model.groups:
        part = Dummy()
        part.start = group.start
        part.count = group.end - group.start + 1
        part.texId = 0

        if group.mat and group.mat.map_Kd:
            part.texId = GL.glGenTextures(1)
            GL.glBindTexture(GL.GL_TEXTURE_2D, part.texId)
            GL.glPixelStorei(GL.GL_UNPACK_ALIGNMENT,1)

            GL.glTexStorage2D(GL.GL_TEXTURE_2D, 4, GL.GL_RGBA8, group.mat.map_Kd.width, group.mat.map_Kd.height)
            GL.glTexSubImage2D(GL.GL_TEXTURE_2D, 0, 0, 0, group.mat.map_Kd.width, group.mat.map_Kd.height, GL.GL_RGBA if group.mat.map_Kd.bpp == 32 else GL.GL_RGB, GL.GL_UNSIGNED_BYTE, group.mat.map_Kd.data)
            GL.glGenerateMipmap(GL.GL_TEXTURE_2D)

            #GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, group.mat.map_Kd.width, group.mat.map_Kd.height, 0,
            #    GL.GL_RGBA if group.mat.map_Kd.bpp == 32 else GL.GL_RGB, GL.GL_UNSIGNED_BYTE, group.mat.map_Kd.data)

            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_REPEAT)
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_REPEAT)
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR)
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR_MIPMAP_LINEAR)
            GL.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_MODULATE)

            GL.glBindTexture(GL.GL_TEXTURE_2D, 0)

        object.parts.append(part)

    return object

def display(screen, shader, object, cam):
    #setup matrices
    projectionMatrix = Matrix44.perspective_projection(45.0, screen.get_height() / screen.get_width(), 1.0, 500.0, dtype='float')
    modelViewMatrix = cam.viewMatrix() * Matrix44.from_scale(Vector3([0.1, 0.1, 0.1]), dtype=numpy.float32)

    GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)
    GL.glUseProgram(shader)

    GL.glUniformMatrix4fv(projMatrixUniform, 1, GL.GL_FALSE, projectionMatrix)
    GL.glUniformMatrix4fv(mvMatrixUniform, 1, GL.GL_FALSE, modelViewMatrix)

    GL.glBindVertexArray( object.vbo )

    GL.glActiveTexture( GL.GL_TEXTURE0 )
    GL.glUniform1i( object.tex0sampler, 0 )

    for part in object.parts:
        GL.glBindTexture( GL.GL_TEXTURE_2D, part.texId )
        GL.glDrawArrays(GL.GL_TRIANGLES, part.start, part.count)

    GL.glBindVertexArray( 0 )

    GL.glUseProgram(0)

def main():
    import loader
    obj = loader.loadObj('sponza.obj', format='vnt')

    camera = cam.Camera(Vector3([0.0, 10.0, 10.0]))

    global projMatrixUniform
    global mvMatrixUniform

    pygame.init()

    screen = pygame.display.set_mode((512, 512), pygame.OPENGL|pygame.DOUBLEBUF, 32)
    GL.glClearColor(0.5, 0.5, 0.5, 1.0)
    GL.glEnable(GL.GL_DEPTH_TEST)
    GL.glDisable(GL.GL_CULL_FACE)
    GL.glEnable(GL.GL_TEXTURE_2D)
    GL.glAlphaFunc(GL.GL_GREATER, 0.0)
    GL.glEnable(GL.GL_ALPHA_TEST)
    GL.glEnable(GL.GL_POLYGON_SMOOTH)

    shader = OpenGL.GL.shaders.compileProgram(
        OpenGL.GL.shaders.compileShader(vertex_shader % "vec3" , GL.GL_VERTEX_SHADER),
        OpenGL.GL.shaders.compileShader(fragment_shader, GL.GL_FRAGMENT_SHADER)
    )

    projMatrixUniform = GL.glGetUniformLocation(shader, 'proj')
    mvMatrixUniform = GL.glGetUniformLocation(shader, "world")

    vertex_array_object = create_object(shader, obj)

    clock = pygame.time.Clock()
    #camera.move(Vector3([0.0, 50.0, 100.0]))

    mousePos = None
    mouseDown = False
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return
            if event.type == pygame.KEYUP and event.key == pygame.K_ESCAPE:
                return
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    mouseDown = True
                    mousePos = map(float, event.pos)
            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1:
                    mouseDown = False
                    mousePos = None
            elif event.type == pygame.MOUSEMOTION:
                if mousePos is not None and mouseDown:
                    diffX = float(event.pos[0]) - mousePos[0]
                    diffY = float(event.pos[1]) - mousePos[1]
                    camera.rotate(diffX * 0.01, -diffY * 0.01)
                    mousePos = map(float, event.pos)

        key = pygame.key.get_pressed()
        if key[pygame.K_w]:
            camera.move(camera.direction * 10.0)
        elif key[pygame.K_s]:
            camera.move(-camera.direction * 10.0)

        display(screen, shader, vertex_array_object, camera)
        pygame.display.flip()

if __name__ == '__main__':
    try:
        main()
    finally:
        pygame.quit()